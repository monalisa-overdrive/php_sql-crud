<!DOCTYPE html>
<html>
  <head>
    <title>API Test</title>
  </head>
  <body>
    <!-- (A) LOGIN -->
    <form method="post" action="users-api.php">
      <input type="text" name="req" value="in" readonly />
      <input type="email" name="email" value="john@doe.com" required />
      <input type="text" name="password" value="123456" required />
      <input type="submit" value="1 - Login"/>
    </form>

    <!-- (B) ADD USER -->
    <form method="post" action="users-api.php">
      <input type="text" name="req" value="save" readonly />
      <input type="email" name="email" value="jane@doe.com" required />
      <input type="text" name="password" value="123456" required />
      <input type="submit" value="2 - Add User"/>
    </form>

    <!-- (C) UPDATE USER -->
    <form method="post" action="users-api.php">
      <input type="text" name="req" value="save" readonly />
      <input type="number" name="id" value="2" readonly />
      <input type="email" name="email" value="janezzzzzzz@doe.com" required />
      <input type="text" name="password" value="12345654321" required />
      <input type="submit" value="3 - Update User"/>
    </form>

    <!-- (D) GET USER -->
    <form method="post" action="users-api.php">
      <input type="text" name="req" value="get" readonly />
      <input type="number" name="id" value="1" required />
      <input type="submit" value="4 - Get User"/>
    </form>

    <!-- (E) DELETE USER -->
    <form method="post" action="users-api.php">
      <input type="text" name="req" value="del" readonly />
      <input type="number" name="id" value="2" required />
      <input type="submit" value="5 - Delete User"/>
    </form>

    <!-- (F) LOGOUT -->
    <form method="post" action="users-api.php">
      <input type="text" name="req" value="out" readonly />
      <input type="submit" value="6 - Logout"/>
    </form>
  </body>
</html>
